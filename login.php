<?php 
  include ('config.php');

  if (isset ($_POST["login"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    //cek username apakah ada di database atau tidak
    $result = mysqli_query($koneksi, "SELECT * FROM user WHERE username = '$username'");
    
    // mysqli_num_rows() untuk mengetahui ada berapa baris data yang dikembalikan
    if (mysqli_num_rows($result) === 1 ) {
        //cek password
        $row = mysqli_fetch_assoc($result);
        //password_verify() untuk mengecek apakah sebuah password itu sama atau tidak dengan hash nya
        //parameternya yaitu string yang belum diacak dan string yang sudah diacak
        if (password_verify($password, $row["pwd"])) {
          setcookie('id', $row['iduser'], time()+10800);
          setcookie('role', hash('ripemd160', $row['rolename']), time()+10800);
          echo "<script>
                  alert('Selamat Datang');
                  document.location.href='index.php';
                </script>";
          exit;
        }
    }
    $error = true;
  }


?>
<!DOCTYPE html>
<html class="background" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Fzimagine Gallery | Login</title>
    <link href="bootstrap-5.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="bootstrap-icons-1.7.2/bootstrap-icons.css" />
    <link rel="stylesheet" href="style.css" />
  </head>

  <body class="background">
    <!-- Membuat Navbar -->
    <nav class="navbar navbar-expand-lg bg-dark text-white fs-5">
      <div class="container">
        <a class="navbar-brand fs-3 fw-bold text-white me-5" href="index.php"><i class="bi bi-camera me-2"></i>Fzimagine Gallery</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse me-3 text-end" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-end">
            <li class="nav-item">
              <a class="nav-link text-white" aria-current="page" href="index.php">Home</a>
            </li>
          </ul>
      </div>
    </nav>
    <!-- Navbar Selesai -->

    <!-- Membuat Form Login -->
    <div class="coba">
      <div class="card login-form">
        <div class="card-body">
          <h1 class="card-title text-center mb-3 login fw-bold">LOGIN</h1>
          <p class="text-center mb-3">Silahkan Login untuk masuk ke aplikasi</p>

          <form action="" method="POST">
            <?php if (isset ($error)) : ?>
              <p style="color: red; font-style: italic;">Username / Password Salah</p>
            <?php endif;?>

            <div class="input-group mb-3">
              <input type="text" class="form-control" placeholder="Username" name="username" />
              <div class="input-group-append">
                <div class="input-group-text">
                  <i class="bi bi-person-fill"></i>
                </div>
              </div>
            </div>

            <div class="input-group mb-3">
              <input type="password" class="form-control" placeholder="Password" name="password" />
              <div class="input-group-append">
                <div class="input-group-text">
                  <i class="bi bi-lock-fill"></i>
                </div>
              </div>
            </div>

            <div class="input-group mb-3 justify-content-end">
              <a href="register.php">Daftar Disini</a>
            </div>

            <button type="submit" class="btn btn-primary panjang" name="login">Submit</button>
          </form>
        </div>
      </div>
    </div>
    <!-- Form Login Selesai -->

    <!-- Membuat Footer -->
    <footer class="text-center mt-5 p-1 bg-secondary text-light fw-bold">
      <p>Copyright &copy; 2022 Create By Fillah Zaki Alhaqi</p>
    </footer>
    <!-- Footer Selesai -->

    <script src="bootstrap-5.2.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-5.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>
