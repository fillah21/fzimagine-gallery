-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Jun 2022 pada 17.34
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fzimagine`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto`
--

CREATE TABLE `foto` (
  `idfoto` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `foto`
--

INSERT INTO `foto` (`idfoto`, `iduser`, `foto`, `judul`, `deskripsi`) VALUES
(12, 12, '62bdbd8984080.jpg', 'Kenny G', 'Pemain saxophone terkenal'),
(13, 12, '62bdbdb61188b.jpg', 'Quotes Kenny G', 'Kata-kata yang sering diucapkan oleh Kenny G'),
(14, 13, '62bdbde688712.png', 'Partitur', 'Partitur musik'),
(15, 13, '62bdbe052c958.png', 'Kress', 'Kress dalam not balok'),
(16, 10, '62bdbe5a4ab47.png', 'Kamus', 'Animasi Kamus'),
(17, 10, '62bdbe88d01cd.jpg', 'Pak Tarno', 'Pesulap terkenal dari Indonesia');

-- --------------------------------------------------------

--
-- Struktur dari tabel `komentar`
--

CREATE TABLE `komentar` (
  `idkomentar` int(11) NOT NULL,
  `idfoto` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `komentar` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `komentar`
--

INSERT INTO `komentar` (`idkomentar`, `idfoto`, `iduser`, `komentar`) VALUES
(14, 12, 10, 'I Like Kenny G'),
(15, 14, 10, 'Ilmu yang bermanfaat, terima kasih'),
(16, 17, 12, 'Saya fans dengan pak tarno'),
(17, 15, 12, 'Mantap sekali'),
(19, 14, 12, 'Nice'),
(20, 16, 12, 'Nice desain'),
(21, 13, 10, 'Sangat Menginspirasi'),
(22, 15, 10, 'Alhamdulillah, ilmu baru'),
(23, 12, 13, 'Nice saxophone'),
(24, 13, 13, 'Love kenny'),
(25, 17, 13, 'Keren'),
(26, 16, 13, 'Bagus sekali');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `rolename` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`iduser`, `username`, `nama`, `email`, `pwd`, `rolename`) VALUES
(10, 'fillah21', 'Fillah Zaki Alhaqi', 'fillah.alhaqi11@gmail.com', '$2y$10$QmKTgJS1.4OBtSwkK3dLNeLO32D.ejUJKz3Bior2oAUeIEFgSDCoq', 'Admin'),
(12, 'admin', 'Admin', 'admin@example.com', '$2y$10$b5wPcQ8GI.jRMNrGxktf8eN81ngzGQSPg900y/atTCXd3YBHZax9i', 'Admin'),
(13, 'member', 'Member', 'member@example.com', '$2y$10$9o36.cg78n7IH2IayhNrRuOG9mwcbCFl1TyiKUtDnRZeRzjLRaWIu', 'Member');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `foto`
--
ALTER TABLE `foto`
  ADD PRIMARY KEY (`idfoto`),
  ADD KEY `iduser` (`iduser`);

--
-- Indeks untuk tabel `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`idkomentar`),
  ADD KEY `idfoto` (`idfoto`),
  ADD KEY `iduser` (`iduser`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `foto`
--
ALTER TABLE `foto`
  MODIFY `idfoto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `komentar`
--
ALTER TABLE `komentar`
  MODIFY `idkomentar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `foto`
--
ALTER TABLE `foto`
  ADD CONSTRAINT `foto_ibfk_1` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `komentar_ibfk_1` FOREIGN KEY (`idfoto`) REFERENCES `foto` (`idfoto`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `komentar_ibfk_2` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
