<?php 
  include ('config.php');
  
  $idfoto = $_GET['idfoto'];

  $query = mysqli_query($koneksi, "SELECT * FROM foto WHERE idfoto = '$idfoto'");
  $row = mysqli_fetch_assoc($query);

  // Membuat fungsi untuk upload foto
  function upload_foto() {
    $namaFile = $_FILES['foto']['name'];
    $ukuranFile = $_FILES ['foto']['size'];
    $error = $_FILES ['foto']['error'];
    $tmpName = $_FILES['foto']['tmp_name'];

    // Cek apakah ada gambar yang diupload atau tidak
    if ($error === 4) {
      echo "<script>
              alert('Silahkan Masukkan Foto');
            </script>";

            if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") {
              echo "<script>
                    document.location.href='detail-admin.php';
                  </script>";
            } else {
              echo "<script>
                      document.location.href='detail-user.php';
                    </script>";
            }
      return false;
    }

    //cek apakah yang di upload gambar atau bukan
    $ekstensiGambarValid = ['jpg', 'jpeg', 'png'];
    $ekstensiGambar = explode('.', $namaFile);
    $ekstensiGambar = strtolower(end($ekstensiGambar));

    //cek apakah ekstensinya ada atau tidak di dalam array $ekstensiGambarValid
    if (!in_array($ekstensiGambar, $ekstensiGambarValid)) {
      echo "<script>
              alert('Yang anda upload bukan gambar');
            </script>";

      if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") {
        echo "<script>
              document.location.href='detail-admin.php';
            </script>";
      } else {
        echo "<script>
                document.location.href='detail-user.php';
              </script>";
      }
      return false;
    }

    //cek jika ukurannya terlalu besar, ukurannya dalam byte
    if($ukuranFile > 5000000) {
      echo "<script>
              alert('Ukuran gambar terlalu besar, jangan melebihi 5mb');
            </script>";
      
            if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") {
              echo "<script>
                    document.location.href='detail-admin.php';
                  </script>";
            } else {
              echo "<script>
                      document.location.href='detail-user.php';
                    </script>";
            }
      
      return false;
    }

    //generate nama gambar baru
    $namaFileBaru = uniqid();
    $namaFileBaru .= '.';
    $namaFileBaru .= $ekstensiGambar;
    //parameternya file namenya, lalu tujuannya
    move_uploaded_file($tmpName, 'img/'.$namaFileBaru);

    return $namaFileBaru;

  }

  if(isset($_POST['submit'])) {
    $fotoLama = $_POST['fotoLama'];
    $judul = $_POST['judul'];
    $deskripsi = $_POST['deskripsi'];

    // Cek apakah user upload foto baru atau tidak
    if($_FILES ['foto']['error'] === 4) {
      $foto = $fotoLama;
    } else {
      $foto = upload_foto();
    }

    mysqli_query($koneksi, "UPDATE foto SET
                  foto = '$foto',
                  judul = '$judul',
                  deskripsi = '$deskripsi'
                  WHERE idfoto='$idfoto'
                  ");
    echo "<script>
            alert('Data Berhasil Diedit');
          </script>";
    
          if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") {
            echo "<script>
                  document.location.href='detail-admin.php';
                </script>";
          } else {
            echo "<script>
                    document.location.href='detail-user.php';
                  </script>";
          }
  }

  
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fzimagine Gallery | Edit Postingan</title>
    <link href="bootstrap-5.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="bootstrap-icons-1.7.2/bootstrap-icons.css" />
    
  </head>

  <body>
    <!-- Membuat Navbar -->
    <nav class="navbar navbar-expand-lg bg-dark text-white fs-5">
      <div class="container">
        <a class="navbar-brand fs-3 fw-bold text-white me-5" href="index.php"><i class="bi bi-camera me-2"></i>Fzimagine Gallery</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse me-3 text-end" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-end">
            <li class="nav-item">
              <a class="nav-link text-white" aria-current="page" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active text-white" aria-current="page" href="tambah-foto.php"><i class="bi bi-plus-lg"></i></a>
            </li>
          </ul>

          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light" type="submit">Search</button>
          </form>
        </div>

        <?php if(isset($_COOKIE['id']) && isset($_COOKIE['role'])) : ?>
          <?php 
            $iduser = $_COOKIE['id'];
            $query = mysqli_query($koneksi, "SELECT * FROM user WHERE iduser = '$iduser'");
            while ($data = mysqli_fetch_assoc($query)) :
          ?>
            <?php if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") :?>
              <a href="detail-admin.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
              <a href="admin.php" class="btn btn-success text-white ms-3" style="text-decoration:none;">Admin</a>
            <?php else : ?>
              <a href="detail-user.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
            <?php endif;?>
          <?php endwhile;?>
        <?php else :?>
          <a href="login.php" class="bg-dark text-white ms-3" style="text-decoration:none;">Anda Belum Login</a>
        <?php endif;?>
            
        
        <a href="logout.php" class="btn btn-danger ms-3" onclick="return confirm('Apakah anda yakin ingin Log Out?')"><i class="bi bi-power"></i></a>
      </div>
    </nav>
    <!-- Navbar Selesai -->

    <!-- Main Content -->
    <div class="container mt-5">
        <h1>Edit Postingan</h1>

        <form action="" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="fotoLama" value="<?php echo $row['foto']?>">
          <div class="mb-3">
            <label for="foto" class="form-label">Foto</label><br>
            <img src="img/<?php echo $row['foto']?>" alt="" width="500">
            <input class="form-control" type="file" id="foto" name="foto" require>
          </div>

          <div class="mb-3">
            <label for="judul" class="form-label">Judul</label>
            <input type="text" class="form-control" id="judul" name="judul" value="<?php echo $row['judul']?>" require>
          </div>

          <div class="mb-3">
            <label for="deskripsi" class="form-label">Deskripsi</label>
            <textarea class="form-control" id="deskripsi" rows="3" name="deskripsi"><?php echo $row['deskripsi']?></textarea>
          </div>

          <button type="submit" class="btn btn-lg btn-success" name="submit">Edit</button>
        </form>
    </div>
    <!-- Main Content Selesai -->

    <!-- Membuat Footer -->
    <footer>
      <nav class="navbar navbar-expand-sm bg-secondary text-white fw-bold justify-content-center mt-5">
        <p>Copyright &copy; 2022 Create By Fillah Zaki Alhaqi</p>
      </nav>
    </footer>
    <!-- Footer Selesai -->

    <script src="bootstrap-5.2.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-5.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>