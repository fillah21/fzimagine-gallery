<?php 
  include ('config.php');

  $query_user = mysqli_query($koneksi, "SELECT * FROM user");
  $query_foto = mysqli_query($koneksi, "SELECT * FROM foto");

  $jumlah_user = mysqli_num_rows($query_user);
  $jumlah_foto = mysqli_num_rows($query_foto);
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fzimagine Gallery | Admin</title>
    <link href="bootstrap-5.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="bootstrap-icons-1.7.2/bootstrap-icons.css" />
    
  </head>

  <body>
    <!-- Membuat Navbar -->
    <nav class="navbar navbar-expand-lg bg-dark text-white fs-5">
      <div class="container">
        <a class="navbar-brand fs-3 fw-bold text-white me-5" href="index.php"><i class="bi bi-camera me-2"></i>Fzimagine Gallery</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse me-3 text-end" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-end">
            <li class="nav-item">
              <a class="nav-link text-white" aria-current="page" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active text-white" aria-current="page" href="tambah-foto.php"><i class="bi bi-plus-lg"></i></a>
            </li>
          </ul>

          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light" type="submit">Search</button>
          </form>
        </div>

        <?php if(isset($_COOKIE['id']) && isset($_COOKIE['role'])) : ?>
          <?php 
            $iduser = $_COOKIE['id'];
            $query = mysqli_query($koneksi, "SELECT * FROM user WHERE iduser = '$iduser'");
            while ($data = mysqli_fetch_assoc($query)) :
          ?>
            <?php if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") :?>
              <a href="detail-admin.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
              <a href="admin.php" class="btn btn-success text-white ms-3" style="text-decoration:none;">Admin</a>
            <?php else : ?>
              <a href="detail-user.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
            <?php endif;?>
          <?php endwhile;?>
        <?php else :?>
          <a href="login.php" class="bg-dark text-white ms-3" style="text-decoration:none;">Anda Belum Login</a>
        <?php endif;?>
            
        
        <a href="logout.php" class="btn btn-danger ms-3" onclick="return confirm('Apakah anda yakin ingin Log Out?')"><i class="bi bi-power"></i></a>
      </div>
    </nav>
    <!-- Navbar Selesai -->

        <!-- Membuat Main Content -->
        <div class="container">
          <!-- Membuat Tabel Data User -->
          <h1 class="mb-3">Data User</h1>

          <h3>Total User : <?php echo $jumlah_user;?></h3>
          <table class="table table-striped table-hover col-auto table-bordered border-dark me-5">
            <thead class="table-dark">
              <tr class="text-center">
                <th width="50">No.</th>
                <th>Username</th>
                <th>Nama</th>
                <th>E-mail</th>
                <th>Password</th>
                <th>Rolename</th>
                <th>Aksi</th>
              </tr>
            </thead>
                
            <tbody>
              <?php 
                $i=1;
                foreach($query_user as $user) :
              ?>
              <tr>
                <td><?php echo $i;?></td>
                <td><?php echo $user['username']?></td>
                <td><?php echo $user['nama']?></td>
                <td><?php echo $user['email']?></td>
                <td><?php echo $user['pwd']?></td>
                <td><?php echo $user['rolename']?></td>
                <td class="text-center">
                  <a href="edit-user.php?iduser=<?php echo $user['iduser']?>" class="bg-success decor text-white ps-1 pt-1 pb-1 me-1">
                    <i class="bi bi-pencil-square"></i>
                    </a> |
                    <a href="hapus-admin.php?iduser=<?php echo $user["iduser"]?>" class="bg-danger decor text-white p-1" onclick="return confirm('Apakah anda yakin ingin menghapus data? Seluruh data anda akan dihapus, termasuk foto dan komentar')">
                      <i class="bi bi-trash"></i>
                    </a>
                </td>
              </tr>
              <?php 
                $i++;
                endforeach;
              ?>
            </tbody>
          </table>
          <!-- Tabel Data User Selesai-->

          <!-- Tabel Data Postingan -->

          <h1 class="mb-3 mt-5">Data Postingan</h1>

          <h3>Total Postingan : <?php echo $jumlah_foto;?></h3>
            <table class="table table-striped table-hover table-bordered border-dark me-5">
                <thead class="table-dark">
                    <tr class="text-center">
                        <th width="50">No.</th>
                        <th>Foto</th>
                        <th>Judul</th>
                        <th>Deskripsi</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                
                <tbody>
                  <?php 
                    $j=1;
                    foreach($query_foto as $foto) :
                  ?>
                    <tr>
                        <td><?php echo $j;?></td>
                        <td>
                          <img src="img/<?php echo $foto["foto"];?>" width="50">
                        </td>
                        <td><?php echo $foto["judul"];?></td>
                        <td><?php echo $foto["deskripsi"];?></td>
                        <td class="text-center">
                            <a href="edit-foto.php?idfoto=<?php echo $foto['idfoto']?>" class="bg-success decor text-white ps-1 pt-1 pb-1 me-1">
                                <i class="bi bi-pencil-square"></i>
                            </a> |
                            <a href="hapus.php?idfoto=<?php echo $foto["idfoto"]?>" class="bg-danger decor text-white p-1" onclick="return confirm('Apakah anda yakin data akan dihapus?')">
                                <i class="bi bi-trash"></i>
                            </a>
                        </td>
                    </tr>
                    <?php 
                      $j++;
                      endforeach;
                    ?>
                </tbody>
            </table>

            <!-- Data Postingan Selesai -->
        </div> 
        <!-- Main Content Selesai -->
    

    

    <!-- Membuat Footer -->
    <footer>
      <nav class="navbar navbar-expand-sm bg-secondary text-white fw-bold justify-content-center">
        <p>Copyright &copy; 2022 Create By Fillah Zaki Alhaqi</p>
      </nav>
    </footer>
    <!-- Footer Selesai -->

    <script src="bootstrap-5.2.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-5.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>