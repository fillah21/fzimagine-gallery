<?php 
  include ('config.php');

  if(!isset($_COOKIE['id']) && !isset($_COOKIE['role'])) {
    echo "<script>
            alert('Silahkan Login terlebih dahulu');
            document.location.href='login.php';
          </script>";
    exit;
  }

  $iduser = $_GET['iduser'];
  $query_user = mysqli_query($koneksi, "SELECT * FROM user WHERE iduser = '$iduser'");
  $row_user = mysqli_fetch_assoc($query_user);

  if(isset($_POST['edit_user'])) {
    $username = strtolower(stripslashes ($_POST["username"]));
    $nama = $_POST["nama"];
    $email = $_POST["email"];
    $rolename = $_POST['rolename'];

    mysqli_query($koneksi, "UPDATE user SET
                  username = '$username',
                  nama = '$nama',
                  email = '$email',
                  rolename = '$rolename'
                  WHERE iduser='$iduser'
                  ");

    echo "<script>
            alert('Data Berhasil Diedit');
          </script>";

          if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") {
            echo "<script>
                  document.location.href='admin.php';
                </script>";
          } else {
            echo "<script>
                    document.location.href='admin.php';
                  </script>";
  }
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fzimagine Gallery | Edit Komentar</title>
    <link href="bootstrap-5.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="bootstrap-icons-1.7.2/bootstrap-icons.css" />
    
  </head>

  <body>
    <!-- Membuat Navbar -->
    <nav class="navbar navbar-expand-lg bg-dark text-white fs-5">
      <div class="container">
        <a class="navbar-brand fs-3 fw-bold text-white me-5" href="index.php"><i class="bi bi-camera me-2"></i>Fzimagine Gallery</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse me-3 text-end" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-end">
            <li class="nav-item">
              <a class="nav-link text-white" aria-current="page" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active text-white" aria-current="page" href="tambah-foto.php"><i class="bi bi-plus-lg"></i></a>
            </li>
          </ul>

          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light" type="submit">Search</button>
          </form>
        </div>

        <?php if(isset($_COOKIE['id']) && isset($_COOKIE['role'])) : ?>
          <?php 
            $iduser = $_COOKIE['id'];
            $query = mysqli_query($koneksi, "SELECT * FROM user WHERE iduser = '$iduser'");
            while ($data = mysqli_fetch_assoc($query)) :
          ?>
            <?php if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") :?>
              <a href="detail-admin.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
              <a href="admin.php" class="btn btn-success text-white ms-3" style="text-decoration:none;">Admin</a>
            <?php else : ?>
              <a href="detail-user.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
            <?php endif;?>
          <?php endwhile;?>
        <?php else :?>
          <a href="login.php" class="bg-dark text-white ms-3" style="text-decoration:none;">Anda Belum Login</a>
        <?php endif;?>
            
        
        <a href="logout.php" class="btn btn-danger ms-3" onclick="return confirm('Apakah anda yakin ingin Log Out?')"><i class="bi bi-power"></i></a>
      </div>
    </nav>
    <!-- Navbar Selesai -->

    <!-- Main Content -->
    <div class="container mt-5">
        <h1>Edit Komentar</h1>

        <form action="" method="POST">
            <div class="mb-3 mt-3">
              <label for="nama" class="form-label">Nama Lengkap</label>
              <input type="text" class="form-control" id="nama" name="nama" value="<?php echo $row_user['nama']?>" require>
            </div>
                        
            <div class="mb-3">
              <label for="username" class="form-label">Username</label>
              <input type="text" class="form-control" id="username" name="username" value="<?php echo $row_user['username']?>" require>
            </div>
                            
            <div class="mb-3">
              <label for="email" class="form-label">E-mail</label>
              <input type="email" class="form-control" id="email" name="email" value="<?php echo $row_user['email']?>" require>
            </div>

            <div class="mb-3">
              <label for="rolename" class="form-label">Role :</label>
              <div class="form-check">
                <input type="radio" class="form-check-input" name="rolename" value="Admin" <?php if($row_user['rolename'] == 'Admin'){ echo 'checked'; } ?> required>
                <label class="form-check-label">Admin</label>
              </div>
              <div class="form-check">
                <input type="radio" class="form-check-input" name="rolename" value="Member" <?php if($row_user['rolename'] == 'Member'){ echo 'checked'; } ?> required>
                <label class="form-check-label">Member</label>
              </div>
            </div>
                        
            <button type="submit" class="btn btn-success" name="edit_user">Edit Data</button>
            <a href="edit-password.php?iduser=<?php echo $_COOKIE["id"]?>" class="btn btn-secondary">Edit Password</a>
            <a href="hapus.php?iduser=<?php echo $row_user["iduser"]?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus data? Seluruh data anda akan dihapus, termasuk foto dan komentar')">Hapus Data</a>
        </form>
    </div>
    <!-- Main Content Selesai -->

    <!-- Membuat Footer -->
    <footer>
      <nav class="navbar navbar-expand-sm bg-secondary text-white fw-bold justify-content-center fixed-bottom">
        <p>Copyright &copy; 2022 Create By Fillah Zaki Alhaqi</p>
      </nav>
    </footer>
    <!-- Footer Selesai -->

    <script src="bootstrap-5.2.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-5.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>