CREATE TABLE `user` (
    `iduser` int(11) primary key auto_increment,
    `username` varchar(50) NOT NULL,
    `nama` varchar(100) NOT NULL,
    `email` varchar(50) NOT NULL,
    `pwd` varchar(255) NOT NULL,
    `rolename` varchar(25) NOT NULL
);

CREATE TABLE `foto` (
    `idfoto` int(11) primary key auto_increment,
    `iduser` int(11) NOT NULL,
    `foto` varchar(100) NOT NULL,
    `judul` varchar(50) NOT NULL,
    `deskripsi` text NOT NULL,
    foreign key(iduser) references user(iduser)
    ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE `komentar` (
    `idkomentar` int(11) primary key auto_increment,
    `idfoto` int(11) NOT NULL,
    `iduser` int(11) NOT NULL,
    `komentar` text NOT NULL,
    foreign key(idfoto) references foto(idfoto) ON DELETE CASCADE ON UPDATE CASCADE,
    foreign key(iduser) references user(iduser) ON DELETE CASCADE ON UPDATE CASCADE
);
