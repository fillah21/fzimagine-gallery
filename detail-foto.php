<?php 
  include ('config.php');

  $idfoto = $_GET['idfoto'];

  if(isset($_COOKIE['id']) && isset($_COOKIE['role'])) {
    $iduser = $_COOKIE['id'];

    if(isset($_POST['tambah'])) {
      $komentar = $_POST['komentar'];
  
      $tambah_komentar = mysqli_query($koneksi, "INSERT INTO komentar VALUES('', '$idfoto', '$iduser', '$komentar')");
  
      if(mysqli_affected_rows($koneksi) > 0) {
        echo "<script>
                alert('Komentar Ditambahkan');
                document.location.href='index.php';
              </script>";
      } else {
        echo "<script>
                alert('Gagal Ditambahkan');
              </script>";
      }
    }
    
  }
  
            
  $query_foto = mysqli_query($koneksi, "SELECT * FROM foto WHERE idfoto = '$idfoto'");
  $foto = mysqli_fetch_assoc($query_foto);

  $query_komentar = mysqli_query($koneksi, "SELECT * FROM komentar WHERE idfoto = '$idfoto'");

  $user = $foto['iduser'];

  $query_user = mysqli_query($koneksi, "SELECT nama FROM user WHERE iduser = '$user'");
  $nama = mysqli_fetch_assoc($query_user);
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fzimagine Gallery | Postingan</title>
    <link href="bootstrap-5.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="bootstrap-icons-1.7.2/bootstrap-icons.css" />
    
  </head>

  <body>
    <!-- Membuat Navbar -->
    <nav class="navbar navbar-expand-lg bg-dark text-white fs-5">
      <div class="container">
        <a class="navbar-brand fs-3 fw-bold text-white me-5" href="index.php"><i class="bi bi-camera me-2"></i>Fzimagine Gallery</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse me-3 text-end" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-end">
            <li class="nav-item">
              <a class="nav-link text-white" aria-current="page" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active text-white" aria-current="page" href="tambah-foto.php"><i class="bi bi-plus-lg"></i></a>
            </li>
          </ul>

          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light" type="submit">Search</button>
          </form>
        </div>

        <?php if(isset($_COOKIE['id']) && isset($_COOKIE['role'])) : ?>
          <?php 
            $query = mysqli_query($koneksi, "SELECT * FROM user WHERE iduser = '$iduser'");
            while ($data = mysqli_fetch_assoc($query)) :
          ?>
            <?php if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") :?>
              <a href="detail-admin.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
              <a href="admin.php" class="btn btn-success text-white ms-3" style="text-decoration:none;">Admin</a>
            <?php else : ?>
              <a href="detail-user.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
            <?php endif;?>
          <?php endwhile;?>
        <?php else :?>
          <a href="login.php" class="bg-dark text-white ms-3" style="text-decoration:none;">Anda Belum Login</a>
        <?php endif;?>
            
        
        <a href="logout.php" class="btn btn-danger ms-3" onclick="return confirm('Apakah anda yakin ingin Log Out?')"><i class="bi bi-power"></i></a>
      </div>
    </nav>
    <!-- Navbar Selesai -->

    <!-- Main Content -->
    <div class="container mt-5">
        <div class="row">
          <img src="img/<?php echo $foto['foto'];?>" alt="">
          <h1>
            <?php echo $foto['judul'];?>
          </h1>
          <h4>
            Diposting Oleh :
            <?php 
              echo $nama['nama'];
            ?>
          </h4>
          <p class="fs-5">
            <?php echo $foto['deskripsi'];?>
          </p>
        </div>

        <!-- Membuat Kolom Komentar -->
        <?php $jumlah_komentar = mysqli_num_rows($query_komentar);?>
        <h3 class="mt-5">Komentar (<?php echo $jumlah_komentar;?>)</h3>
        
        <?php 
          $i=1;
          foreach ($query_komentar as $komen) :
            $idnama = $komen['iduser'];
            $nama_komentar = mysqli_query($koneksi, "SELECT nama FROM user WHERE iduser = '$idnama'");
            $row = mysqli_fetch_assoc($nama_komentar);
        ?>
          <div class="card mb-2">
            <div class="card-body">
              <h5 class="card-title"><?php echo $row['nama']?></h5>
              <p class="card-text"><?php echo $komen['komentar']?></p>
            </div>
          </div>
        <?php 
          $i++;
          endforeach;
        ?>
        <!-- Kolom Komentar Selesai -->
        
        <?php if(isset($_COOKIE['id']) && isset($_COOKIE['role'])) :?>
          <!-- Membuat Masukan Komentar -->
          <form action="" method="POST">
            <div class="mt-5 mb-3">
              <label for="komentar" class="form-label">Masukkan Komentar Anda :</label>
              <textarea class="form-control" id="komentar" rows="3" name="komentar"></textarea>

              <button type="submit" class="btn btn-primary mt-3" name="tambah">Tambah</button>
            </div>
          </form>
          <!-- Masukan Komentar Selesai -->
        <?php endif;?>
    </div>
    <!-- Main Content Selesai -->

    <!-- Membuat Footer -->
    <footer class="text-center mt-5 p-1 bg-secondary text-light fw-bold">
      <p>Copyright &copy; 2022 Create By Fillah Zaki Alhaqi</p>
    </footer>
    <!-- Footer Selesai -->

    <script src="bootstrap-5.2.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-5.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>