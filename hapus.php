<?php 
    include ('config.php');

    if (isset($_GET['iduser'])) {
        function hapus_user($iduser) {
            global $koneksi;
            mysqli_query($koneksi, "DELETE FROM user WHERE iduser = '$iduser'");
    
            return mysqli_affected_rows($koneksi);
        }
    
        $iduser = $_GET['iduser'];
        if (hapus_user($iduser) > 0) {
            setcookie('id', '', time()-3600);
            setcookie('role', '', time()-3600);
            echo "
                <script>
                    alert('Data Berhasil Dihapus');
                    document.location.href='index.php';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('Data Berhasil Dihapus');
                    document.location.href='index.php';
                </script>
            ";
        }
    } elseif (isset($_GET['idfoto'])) {
        function hapus_foto($idfoto) {
            global $koneksi;
            mysqli_query($koneksi, "DELETE FROM foto WHERE idfoto = '$idfoto'");
    
            return mysqli_affected_rows($koneksi);
        }
    
        $idfoto = $_GET['idfoto'];
        if (hapus_foto($idfoto) > 0) {
            echo "
                <script>
                    alert('Data Berhasil Dihapus');
                    document.location.href='index.php';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('Data Berhasil Dihapus');
                    document.location.href='index.php';
                </script>
            ";
        }
    } elseif (isset($_GET['idkomentar'])) {
        function hapus_komentar($idkomentar) {
            global $koneksi;
            mysqli_query($koneksi, "DELETE FROM komentar WHERE idkomentar = '$idkomentar'");
    
            return mysqli_affected_rows($koneksi);
        }
    
        $idkomentar = $_GET['idkomentar'];
        if (hapus_komentar($idkomentar) > 0) {
            echo "
                <script>
                    alert('Data Berhasil Dihapus');
                    document.location.href='index.php';
                </script>
            ";
        } else {
            echo "
                <script>
                    alert('Data Berhasil Dihapus');
                    document.location.href='index.php';
                </script>
            ";
        }
    }

    


?>