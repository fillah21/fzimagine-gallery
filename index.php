<?php 
  include ('config.php');
?>



<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fzimagine Gallery</title>
    <link href="bootstrap-5.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="bootstrap-icons-1.7.2/bootstrap-icons.css" />
    
  </head>

  <body>
    <!-- Membuat Navbar -->
    <nav class="navbar navbar-expand-lg bg-dark text-white fs-5">
      <div class="container">
        <a class="navbar-brand fs-3 fw-bold text-white me-5" href="index.php"><i class="bi bi-camera me-2"></i>Fzimagine Gallery</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse me-3 text-end" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-end">
            <li class="nav-item">
              <a class="nav-link text-white" aria-current="page" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active text-white" aria-current="page" href="tambah-foto.php"><i class="bi bi-plus-lg"></i></a>
            </li>
          </ul>

          <form class="d-flex" role="search" action="" method="POST">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name="keyword">
            <button class="btn btn-outline-light" type="submit" name="cari">Search</button>
          </form>
        </div>

        <!-- Pengecekan apakah user sudah login atau belum -->
        <?php if(isset($_COOKIE['id']) && isset($_COOKIE['role'])) : ?>
          <?php 
            $iduser = $_COOKIE['id'];
            $query = mysqli_query($koneksi, "SELECT * FROM user WHERE iduser = '$iduser'");
            while ($data = mysqli_fetch_assoc($query)) :
          ?>
            <?php if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") :?>
              <a href="detail-admin.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>  
              <a href="admin.php" class="btn btn-success text-white ms-3" style="text-decoration:none;">Admin</a>
            <?php else : ?>
              <a href="detail-user.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
            <?php endif;?>
          <?php endwhile;?>
        <?php else :?>
          <a href="login.php" class="bg-dark text-white ms-3" style="text-decoration:none;">Anda Belum Login</a>
        <?php endif;?>
            
        
        <a href="logout.php" class="btn btn-danger ms-3" onclick="return confirm('Apakah anda yakin ingin Log Out?')"><i class="bi bi-power"></i></a>
      </div>
    </nav>
    <!-- Navbar Selesai -->

    <!-- Main Content -->
    <section class="py-5 text-center container">
      <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
          <h1 class="fw-light">Fzimagine Gallery</h1>
          <p class="lead text-muted">Abadikan momenmu, dan bagikan kebahagiaan kepada yang lain dengan cara yang menyenangkan!!!</p>
          <p>
            <a href="tambah-foto.php" class="btn btn-primary my-2">Unggah Fotomu</a>
          </p>
        </div>
      </div>
    </section>

    <!-- Membuat Card Foto -->
    <div class="container">
      <div class="row">
        <?php 
          $query = mysqli_query($koneksi, "SELECT * FROM foto ORDER BY idfoto DESC");

          if(isset($_POST['cari'])) {
            $keyword = $_POST['keyword'];
        
            $query = mysqli_query($koneksi, "SELECT * FROM foto WHERE judul LIKE '%$keyword%' OR deskripsi LIKE '%$keyword%'");
            
          }

          $i=1;
          foreach ($query as $foto) :
        ?>
          <div class="col-md-3">
            <div class="card-deck">
              <div class="card">
                <img src="img/<?php echo $foto["foto"]?>" class="card-img-top" alt="...">
                <div class="card-body">
                  <a href="detail-foto.php?idfoto=<?php echo $foto["idfoto"]?>" class="text-dark fs-4" style="text-decoration:none;"><?php echo $foto["judul"]?></a>
                  <p class="card-text"><?php echo $foto["deskripsi"]?></p>
                  <a href="detail-foto.php?idfoto=<?php echo $foto["idfoto"]?>" class="btn btn-primary">Komentar</a>
                </div>
              </div>
            </div>
          </div>
        <?php 
          $i++;
          endforeach;
        ?>
      </div>
    </div>
    <!-- Card Foto Selesai -->
    <!-- Main Content Selesai -->

    <!-- Membuat Footer -->
    <footer class="text-center mt-5 p-1 bg-secondary text-light fw-bold">
      <p>Copyright &copy; 2022 Create By Fillah Zaki Alhaqi</p>
    </footer>
    <!-- Footer Selesai -->

    <script src="bootstrap-5.2.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-5.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>