<?php 
    include ('config.php');

    if(!isset($_COOKIE['id']) && !isset($_COOKIE['role'])) {
        echo "<script>
                alert('Silahkan Login terlebih dahulu');
                document.location.href='login.php';
            </script>";
        exit;
    }

    $iduser = $_GET['iduser'];

    $query_user = mysqli_query($koneksi, "SELECT * FROM user WHERE iduser = '$iduser'");
    $row = mysqli_fetch_assoc($query_user);

    if(isset($_POST['submit'])) {
        $iduser = $_GET['iduser'];
        $password = mysqli_real_escape_string($koneksi, $_POST["password"]);
        $password2 = mysqli_real_escape_string($koneksi, $_POST["password2"]);

        //cek konfirmasi password 1 dan password 2 sama atau tidak
        if ($password !== $password2) {
            echo "<script>
                    alert('Password Tidak Sesuai!');
                    document.location.href='detail-user.php';
                </script>";
            return false;
        }
        //enskripsi password
        //fungsi password_hash() untuk mengacak password
        //parameternya adalah password apa yang mau diacak dan mengacaknya pakai algoritma apa
        $password = password_hash($password2, PASSWORD_DEFAULT);
        
        //jika password sama, masukkan data ke database
        mysqli_query($koneksi, "UPDATE user SET
                    pwd = '$password'
                    WHERE iduser='$iduser'
                    ");
        echo "<script>
                alert('Data Berhasil Diedit');
              </script>";

              if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") {
                echo "<script>
                      document.location.href='detail-admin.php';
                    </script>";
              } else {
                echo "<script>
                        document.location.href='detail-user.php';
                      </script>";
              }
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fzimagine Gallery | Edit Postingan</title>
    <link href="bootstrap-5.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="bootstrap-icons-1.7.2/bootstrap-icons.css" />
    
  </head>

  <body>
    <!-- Membuat Navbar -->
    <nav class="navbar navbar-expand-lg bg-dark text-white fs-5">
      <div class="container">
        <a class="navbar-brand fs-3 fw-bold text-white me-5" href="index.php"><i class="bi bi-camera me-2"></i>Fzimagine Gallery</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse me-3 text-end" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-end">
            <li class="nav-item">
              <a class="nav-link text-white" aria-current="page" href="index.php">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active text-white" aria-current="page" href="tambah-foto.php"><i class="bi bi-plus-lg"></i></a>
            </li>
          </ul>

          <form class="d-flex" role="search">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-light" type="submit">Search</button>
          </form>
        </div>

        <?php if(isset($_COOKIE['id']) && isset($_COOKIE['role'])) : ?>
          <?php 
            $iduser = $_COOKIE['id'];
            $query = mysqli_query($koneksi, "SELECT * FROM user WHERE iduser = '$iduser'");
            while ($data = mysqli_fetch_assoc($query)) :
          ?>
            <?php if($_COOKIE['role'] === "ee712adaef95da67148d2688726bfbf32cee7282") :?>
              <a href="detail-admin.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
              <a href="admin.php" class="btn btn-success text-white ms-3" style="text-decoration:none;">Admin</a>
            <?php else : ?>
              <a href="detail-user.php" class="bg-dark text-white ms-3" style="text-decoration:none;"><?php echo $data["nama"]?></a>
            <?php endif;?>
          <?php endwhile;?>
        <?php else :?>
          <a href="login.php" class="bg-dark text-white ms-3" style="text-decoration:none;">Anda Belum Login</a>
        <?php endif;?>
            
        
        <a href="logout.php" class="btn btn-danger ms-3" onclick="return confirm('Apakah anda yakin ingin Log Out?')"><i class="bi bi-power"></i></a>
      </div>
    </nav>
    <!-- Navbar Selesai -->

    <!-- Main Content -->
    <div class="container mt-5">
        <h1>Edit Password</h1>

        <form action="" method="POST">
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="password" name="password" value="<?php echo $row['pwd']?>" require>
            </div>

            <div class="mb-3">
                <label for="password2" class="form-label">Konfirmasi Password</label>
                <input type="password" class="form-control" id="password2" name="password2" value="<?php echo $row['pwd']?>" require>
            </div>

            <button type="submit" class="btn btn-success" name="submit">Edit</button>
        </form>
    </div>
    <!-- Main Content Selesai -->

    <!-- Membuat Footer -->
    <footer>
      <nav class="navbar navbar-expand-sm bg-secondary text-white fw-bold justify-content-center fixed-bottom">
        <p>Copyright &copy; 2022 Create By Fillah Zaki Alhaqi</p>
      </nav>
    </footer>
    <!-- Footer Selesai -->

    <script src="bootstrap-5.2.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-5.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>