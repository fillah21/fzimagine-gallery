<?php 
  include ("config.php");

  if (isset($_POST["submit"])) {
    $username = strtolower(stripslashes ($_POST["username"]));
    $nama = ($_POST["nama"]);
    $email = ($_POST["email"]);
    $password = mysqli_real_escape_string($koneksi, $_POST["password"]);
    $password2 = mysqli_real_escape_string($koneksi, $_POST["password2"]);
    $rolename = "Member";

    //cek username sudah ada atau belum
    $result = mysqli_query($koneksi, "SELECT username FROM user WHERE username = '$username'") or die(mysqli_error($koneksi));
    if (mysqli_fetch_assoc($result)) {
        echo "<script>
                alert('Username Sudah Dipakai!!!');
                document.location.href='register.php';
              </script>";
        return false;
    }

    //cek konfirmasi password 1 dan password 2 sama atau tidak
    if ($password !== $password2) {
        echo "<script>
                alert('Password Tidak Sesuai!');
                document.location.href='register.php';
              </script>";
        return false;
    }
    //enskripsi password
    //fungsi password_hash() untuk mengacak password
    //parameternya adalah password apa yang mau diacak dan mengacaknya pakai algoritma apa
    $password = password_hash($password2, PASSWORD_DEFAULT);
    
    //jika password sama, masukkan data ke database
    mysqli_query($koneksi, "INSERT INTO user VALUES ('', '$username', '$nama', '$email', '$password', '$rolename')");
    echo "<script>
            alert('Data Berhasil Disimpan, dan Silahkan Login');
            document.location.href='login.php';
          </script>";
    return mysqli_affected_rows($koneksi);
  }
?>



<!DOCTYPE html>
<html class="background" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Fzimagine Gallery | Register</title>
    <link href="bootstrap-5.2.0/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="bootstrap-icons-1.7.2/bootstrap-icons.css" />
    <link rel="stylesheet" href="style.css" />
  </head>

  <body class="background">
    <!-- Membuat Navbar -->
    <nav class="navbar navbar-expand-lg bg-dark text-white fs-5">
      <div class="container">
        <a class="navbar-brand fs-3 fw-bold text-white me-5" href="index.php"><i class="bi bi-camera me-2"></i>Fzimagine Gallery</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse me-3 text-end" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0 text-end">
            <li class="nav-item">
              <a class="nav-link text-white" aria-current="page" href="index.php">Home</a>
            </li>
          </ul>
      </div>
    </nav>
    <!-- Navbar Selesai -->

    <!-- Membuat Form Register -->
    <div class="coba">
      <div class="card login-form">
        <div class="card-body">
          <h1 class="card-title text-center mb-3 login fw-bold">REGISTER</h1>
          <p class="text-center mb-3">Silahkan isi data diri untuk mendaftar</p>

          <form action="" method="POST">
            <div class="mb-3">
                <label for="nama" class="form-label">Nama Lengkap</label>
                <input type="text" class="form-control" id="nama" name="nama">
            </div>
            
            <div class="mb-3">
                <label for="username" class="form-label">Username</label>
                <input type="text" class="form-control" id="username" name="username">
            </div>
            
            <div class="mb-3">
                <label for="email" class="form-label">E-mail</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            
            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>

            <div class="mb-3">
                <label for="password2" class="form-label">Konfirmasi Password</label>
                <input type="password" class="form-control" id="password2" name="password2">
            </div>

            <div class="input-group mb-3 justify-content-end">
              <a href="login.php">Sudah Punya Akun?</a>
            </div>

            <button type="submit" class="btn btn-primary panjang" name="submit">Submit</button>
          </form>
        </div>
      </div>
    </div>
    <!-- Form Register Selesai -->

    <!-- Membuat Footer -->
    <footer class="text-center mt-5 p-1 bg-secondary text-light fw-bold">
      <p>Copyright &copy; 2022 Create By Fillah Zaki Alhaqi</p>
    </footer>
    <!-- Footer Selesai -->

    <script src="bootstrap-5.2.0/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.5/dist/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="bootstrap-5.2.0/js/bootstrap.min.js" crossorigin="anonymous"></script>
  </body>
</html>
